import React, { Component } from "react";
import Table from "./Table";
import Form from "./Form";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            characters: []
        };

        // this.removeCharacter = this.removeCharacter.bind(this); // => if using arrow function this not necessary
    }

    removeCharacter = index => {
        const { characters } = this.state;

        this.setState({
            characters: characters.filter((character, i) => {
                return i !== index;
            })
        });
    };

    handleSubmit = character => {
        this.setState({
            characters: [...this.state.characters, character]
        });
    };

    render() {
        return (
            <div className="container">
                <Table
                    characterData={this.state.characters}
                    removeCharacter={this.removeCharacter}
                />
                <Form handleSubmit={this.handleSubmit} />
            </div>
        );
    }
}

export default App;
